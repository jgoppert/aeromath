#include <iostream>
#include <aeromath/Vector3.hpp>
#include <aeromath/EulerAngles.hpp>
#include <aeromath/Dcm.hpp>
#include <aeromath/Matrix.hpp>
#include <aeromath/Quaternion.hpp>
#include <aeromath/Vector.hpp>

int main (int argc, char const* argv[])
{
    using namespace math;
    if (vector3Test()) return -1;
    if (eulerAnglesTest()) return -1;
    if (dcmTest()) return -1;
    if (matrixTest()) return -1;
    if (quaternionTest()) return -1;
    if (vectorTest()) return -1;
    return 0;
}

// vim:ts=4:sw=4:expandtab
