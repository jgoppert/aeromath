include_directories(generic)

set(math_srcs
    test/test.cpp
    Vector.cpp
    Vector3.cpp
    EulerAngles.cpp
    Quaternion.cpp
    Dcm.cpp
    Matrix.cpp
    )

set(math_generic_srcs
    generic/Vector.cpp
    generic/Matrix.cpp
    )

set(math_hdrs
    test/test.hpp
    Vector.hpp
    Vector3.hpp
    EulerAngles.hpp
    Quaternion.hpp
    Dcm.hpp
    Matrix.hpp
    )

set(math_generic_hdrs
    generic/Vector.hpp
    generic/Matrix.hpp
    )

add_library(aeromath
    ${math_srcs}
    ${math_generic_srcs}
    )

add_library(aeromathDP
    ${math_srcs}
    ${math_generic_srcs}
    )
set_target_properties(aeromathDP PROPERTIES COMPILE_FLAGS -DAEROMATH_DOUBLE_PRECISION) 

# vim:ts=4:sw=4:expandtab
