include(CMakeParseArguments)
set(CMAKE_CXX_COMPILER goto-cc)
set(CMAKE_C_COMPILER goto-cc)
add_definitions("-DCBMC_VERIFICATION")
function(CBMC_CHECK_MODEL)
	set(options)
	set(oneValueArgs TARGET)
	set(multiValueArgs INSTRUMENT_OPTS CBMC_OPTS)
	cmake_parse_arguments(PARSE "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})
	if ("${PARSE_TARGET}" STREQUAL "")
		message(FATAL_ERROR "must specify TARGET")
	endif()
	add_custom_command(OUTPUT ${PARSE_TARGET}.inst
		COMMAND goto-instrument ${PARSE_INSTRUMENT_OPTS} 
			${PARSE_TARGET} ${PARSE_TARGET}.inst
		DEPENDS ${PARSE_TARGET})
	add_custom_target(${PARSE_TARGET}_check ALL 
		cbmc ${PARSE_TARGET}.inst ${PARSE_CBMC_OPTS}
		DEPENDS ${PARSE_TARGET}.inst)
endfunction(CBMC_CHECK_MODEL)
